import {Customer} from './Modules/Customer.js';
import {Computer} from './Modules/Computer.js';

/*Creating a customer object  */ 
let customer=new Customer(0,0,0,false); /*Creating a customer*/ 
/*Creating computer elements */
let HPOmenComputer=new Computer("HPOmen", "8299","This bad boy is an awesome gaming machine for the money","https://www.elkjop.no/image/dv_web_D180001002456611/174718/hp-omen-0802-156-baerbar-gaming-pc-mica-silver.jpg?$prod_all4one$","Intel core I7, 16GB ram, 512GB SSD");
let LenovoComputer=new Computer("Lenovo-Ideapad","3999", "This machine is not really suited for gaming, but look at the priiiice","https://www.netonnet.no/GetFile/ProductImagePrimary/data-og-nettbrett/laptop/laptop-14-16-tommer/lenovo-ideapad-s145-14igm-81mw0043mx(1012230)_383059_1_Normal_Large.jpg","Intel celeron, 2GB Ram, 128GB HDD");
let eMacG4=new Computer("eMacG4","14999","This old Mac is suited for all fancy hipsters who wants to stand out, maybe able to run minecraft on ultra low settings.","https://everymac.com/images/cpu_pictures/apple_emac.jpg","G4-processor, 256MB Ram, 40GB hard-drive");
let AcerNitro=new Computer("Acer-Nitro","12999","You like RGB and lighting?? This is the machine for you!","https://www.elkjop.no/image/dv_web_D180001002451406/168738/acer-nitro-5-173-baerbar-gaming-pc.jpg?$prod_all4one$","Intel core I9, 16GB RAM, 512 GB harddrive and lots of LED's") ;

/*A list for reference*/
let listofComputers=[HPOmenComputer,LenovoComputer,eMacG4,AcerNitro];

/*Populating dropdown*/ 
let dropdownbar=document.getElementById("computers-dropdown");
for (let i = 0; i < listofComputers.length; i++) {
   let option = document.createElement('option');
    option.text = listofComputers[i].name;
    option.value = i;
    dropdownbar.add(option);
 }

 /*Getting the selected value from dropdown and adding the description to screen*/
 const elDropdown=document.getElementById("computers-dropdown")
 elDropdown.onchange=function(){
    let strUser = dropdownbar.options[dropdownbar.selectedIndex].value;
    let summaryOfComputer=document.getElementById("body-description");
    summaryOfComputer.innerHTML=listofComputers[strUser].specifications;
   updatePicture(strUser);
    updateSummary(strUser);
    updatePrice(strUser);
 }
/*Adding picture based on which computer which is choosen*/
function updatePicture(dropdownIndex){
let pictureOfComputer=document.getElementById("computer-picture");
let computerImage=document.getElementById("computer-image");
 computerImage.setAttribute("width", "100");
 computerImage.src=listofComputers[dropdownIndex].imageSrc;
 computerImage.innerHTML="";
 pictureOfComputer.appendChild(computerImage);
}
/*Adding summary of machines based on choosen dropdown*/
function updateSummary(dropdownIndex){
    const summaryParagraph=document.getElementById("summary-paragraph");
    summaryParagraph.innerHTML=listofComputers[dropdownIndex].features;
}
/*Updating the price based on which computer is chosen*/ 
function updatePrice(dropdownIndex){
        const priceParagraph=document.getElementById("computer-price");
        priceParagraph.innerHTML=listofComputers[dropdownIndex].price;
    }

 /*Done with setting up the design part. Now adding the functionality*/
    const elLoanButton=document.getElementById("loan-button");
    elLoanButton.onclick=function (){
    let amount = prompt("How much do you want to loan?", "Amount");
    if(amount=="" || amount=="Amount"){
        alert("You must enter a value");
       return;
    }
    if(amount>customer.balance*2){
            alert("You can't get a loan, because you have to little balance");
    }
    else if(customer.loanedBefore===false && amount<=customer.balance*2) {
        let customerBalanceInt= parseInt(customer.balance);
        let amountInt=parseInt(amount);
        customer.balance=customerBalanceInt+amountInt;
        alert(`You got a loan, your new balance is updated,
        your new balance is ${customer.balance}`);
        customer.loanedBefore=true;
        let balanceAmount=document.getElementById("balance-amount");
        balanceAmount.innerHTML=customer.balance+" KR ";
       
    }
    else if(customer.loanedBefore===true){
        alert(`You have already gotten a loan`);
    }
 }
/*Button for work*/ 
const elWorkButton=document.getElementById('work-button');
 elWorkButton.onclick=function(){
    customer.pay+=100;
    let payAmount=document.getElementById("pay-amount");
    payAmount.innerHTML=customer.pay+" KR ";
 }
  
 const elBankButton=document.getElementById('bank-button');
 elBankButton.onclick=function(){
     customer.balance+=customer.pay;
     let balanceAmount=document.getElementById("balance-amount");
     balanceAmount.innerHTML=customer.balance+" KR ";
     let payAmount=document.getElementById("pay-amount");
     customer.pay=0;
     payAmount.innerHTML=customer.pay+" KR ";
 }
 const elBuyButton=document.getElementById("buy-button");
 elBuyButton.onclick=function(){
    let dropdownbar=document.getElementById("computers-dropdown");
    let indexOfMachine = dropdownbar.options[dropdownbar.selectedIndex].value;
    let price=listofComputers[indexOfMachine].price;
    if(price>customer.balance){
        alert('You can not afford this machine yet');
    }
    else{
        alert('Congratulations, the machine is yours! ');
        customer.balance=0;
        customer.loan=0;
        customer.loanedBefore=false;
        customer.pay=0;
        let balanceAmount=document.getElementById("balance-amount");
        balanceAmount.innerHTML="";
    }
 }

 
 

